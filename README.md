# Simple Quiz App [2019]

Created a Simple Wing Tsun Quiz App

## App Overview

### Questions Screen

![Questions Screen](/images/readme/questions_screen.png "Questions Screen")

### Restart Screen

![Restart Screen](/images/readme/restart_screen.png "Restart Screen")
