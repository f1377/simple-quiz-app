import 'package:flutter/material.dart';

class Result extends StatelessWidget {
  final int resultScore;
  final void Function() resetHandler;

  Result(this.resultScore, this.resetHandler);

  String get resultPhrase {
    String resultText = 'You are so bad!';
    if (resultScore == 30) {
      resultText = 'You know everything!';
    } else if (resultScore == 20) {
      resultText = 'Not bad two out of three!';
    } else if (resultScore == 10) {
      resultText = 'Hm one out of three!';
    }

    return resultText;
  }

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Column(
        children: <Widget>[
          Text(
            resultPhrase,
            style: TextStyle(
              fontSize: 36,
              fontWeight: FontWeight.bold,
            ),
            textAlign: TextAlign.center,
          ),
          TextButton(
            child: Text(
              'Restart Quiz!',
              style: TextStyle(
                fontSize: 28,
                backgroundColor: Colors.orange[50],
                fontWeight: FontWeight.bold,
              ),
            ),
            onPressed: resetHandler,
          ),
        ],
      ),
    );
  }
}
